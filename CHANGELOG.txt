Link: redirect formatter 7.x-1.x, xxxx-xx-xx
--------------------------------------------

Features:

* Adds a field formatter named "Redirect to link URL" to allow a link field to
  redirect users to the link field's URL.
* Users with the permission "Skip link redirection" will see a notice instead.
