CONTENTS OF THIS FILE
---------------------

* Introduction
* Requirements
* Recommended modules
* Installation
* Troubleshooting
* FAQ

INTRODUCTION
------------

This module defines a link field formatter which redirects users to the link's
location. This is useful, for example, if you want to post a news article on
your site which, when clicked on, redirects visitors to that news article on a
partner's website.

This module is designed to be pretty powerful, and using it in the wrong way
could cause security issues or unwanted behavior! Please note:

**Warning**: malicious editors could use this formatter to redirect users to
malicious sites! You are responsible for ensuring that only trusted roles can
modify any entity, entity type (content type), and/or field which uses this
formatter, either using Drupal core's built-in permission scheme, or a module
like Field Permissions (see the "Recommended modules" section below).

You need to be especially careful of this for comments and/or user accounts:
without the Field Permissions module, anyone with a user account could redirect
other users to pretty much anywhere.

**Warning**: visitors who have disabled automatic redirection in their browser,
and users with the permission "Redirect to link URL" (defined by this module)
will see any page display where this formatter is being used! You are
responsible for ensuring that sensitive information is hidden.

**Note**: You probably only want to use this formatter on an entity / entity-
type's "Full content" display (that is to say, you probably don't want to use
this formatters on displays that could be displayed in a list). Otherwise, users
without the "Redirect to link URL" permission would be immediately redirected to
the URL of the first list item as soon as they try to view the list.

REQUIREMENTS
------------

The Link module (https://www.drupal.org/project/link), version 7.x-1.0 or
higher.

RECOMMENDED MODULES
-------------------

The Field Permissions module (https://www.drupal.org/project/field_permissions)
can be used to restrict view and/or edit access on sensitive fields to trusted
roles.

The Rabbit Hole module (https://www.drupal.org/project/rabbit_hole) can be used
to redirect users to any path or external URL if they try to view any entity of
a certain type (e.g.: you have a content type that never should be displayed on
its own page, like an image content type that's displayed in a carousel).

INSTALLATION
------------

1. Download and install the link_redirect_formatter project and its
   dependencies:

   See https://www.drupal.org/node/895232 for more information.

2. Choose which users should be allowed to skip redirection by granting the
   "Skip link redirection" permission.

   You probably want to grant this permission to users who are allowed to edit
   entities with link fields that use this formatter. You probably do not want
   to grant this permission to anonymous users: while this has no security
   implications, doing so essentially negates the functionality of this module.

3. Edit the display for an entity type (content type, comment, taxonomy
   vocabulary, user account) which has a link field. In the "Format" column for
   the link field, choose the "Redirect to link URL". Take heed of the warnings
   displayed in the summary column, and check the permissions for this content
   type. Click the "Edit [settings]" gear to change which HTTP code is used to
   redirect the user, if necessary. Click "Save" at the bottom of the page.

TROUBLESHOOTING
--------------

We don't know of any problems at this time, so if you find one, please let us
know by adding an issue!

MAINTAINERS
-----------

Current maintainers:

 * M Parker (mparker17) - https://www.drupal.org/u/mparker17

This project has been sponsored by:

 * Digital Echidna - https://www.drupal.org/digital-echidna
